const Version = '!2.48.4!'.replace(/!/g, ''); // x-release-please-version

export default Version;
